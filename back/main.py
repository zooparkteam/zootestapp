import bottle
from beaker.middleware import SessionMiddleware

# Import all sub-apps
import auth


root = bottle.Bottle()

root.mount('/auth', auth.app)
# root.mound('/subapp', subapp.app)
# ...


# ================ MIDDLEWARE ================ #

session_opts = {
    'session.type': 'file',
    'session.data_dir': './data',
    'session.auto': True
}

root = SessionMiddleware(root, session_opts)


if __name__ == "__main__":
    bottle.run(root)
